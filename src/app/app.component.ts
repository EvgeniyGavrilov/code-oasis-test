import {Component} from '@angular/core';

export type Suit = 'Spades' | 'Hearts' | 'Diamonds' | 'Clubs';
export type CardValue = '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | 'J' | 'Q' | 'K' | 'A';
export interface Card {suit: Suit, value: CardValue}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  globalDealerScore = 0
  globalPlayerScore = 0
  deck: Card[] = [];
  playerCards:  Card[] = [];
  dealerCards:  Card[] = [];
  playerScore = 0;
  dealerScore = 0;
  isPlayerTurn = true;
  isDealerTurn = false;
  isGameOver = false;

  constructor() { }

  ngOnInit(): void {
    this.startNewGame()
  }

  startNewGame() {
    this.deck = [];
    this.playerCards = [];
    this.dealerCards = [];
    this.playerScore = 0;
    this.dealerScore = 0;
    this.isPlayerTurn = true;
    this.isDealerTurn = false;
    this.isGameOver = false;
    this.createDeck();
    this.shuffleDeck();
    this.dealCards();
  }

  createDeck() {
    const suits: Suit[] = ['Spades', 'Hearts', 'Diamonds', 'Clubs'];
    const values: CardValue[] = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    for (let suit of suits) {
      for (let value of values) {
        this.deck.push({ value: value, suit: suit });
      }
    }
  }

  shuffleDeck() {
    for (let i = this.deck.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this.deck[i], this.deck[j]] = [this.deck[j], this.deck[i]];
    }
  }

  dealCards() {
    for(let i = 0; i < 4; i++) {
      const card: Card | undefined = this.deck.pop();
      if(card) {
        i % 2 ? this.playerCards.push(card) :  this.dealerCards.push(card)
      }
    }
    this.calculateScores();
  }

  hit() {
    if (this.isPlayerTurn && !this.isGameOver) {
      const card: Card | undefined = this.deck.pop();
      if(card) {
        this.playerCards.push(card);
        this.calculateScores();
        if (this.playerScore > 21) {
          ++this.globalDealerScore
          this.isGameOver = true;
        }
      }
    }
  }

  stand() {
    if (this.isPlayerTurn && !this.isGameOver) {
      this.isPlayerTurn = false;
      this.isDealerTurn = true;
      while (this.dealerScore <= 16) {
        const card: Card | undefined = this.deck.pop();
        if(card) {
          this.dealerCards.push(card);
          this.calculateScores();
        }
      }
      if((this.dealerScore > 21 || this.dealerScore < this.playerScore) && this.playerScore <= 21) {
        ++this.globalPlayerScore
      } else if (this.dealerScore > this.playerScore) {
        ++this.globalDealerScore
      }
      this.isGameOver = true;
    }
  }

  calculateScores() {
    let playerScore = 0;
    let dealerScore = 0;
    for (let card of this.playerCards) {
      playerScore += this.getCardValue(card.value);
    }
    for (let card of this.dealerCards) {
      dealerScore += this.getCardValue(card.value);
    }
    if (playerScore > 21) {
      for (let card of this.playerCards) {
        if (card.value === 'A') {
          playerScore -= 10;
          if (playerScore <= 21) {
            break;
          }
        }
      }
    }
    if (dealerScore > 21) {
      for (let card of this.dealerCards) {
        if (card.value === 'A') {
          dealerScore -= 10;
          if (dealerScore <= 21) {
            break;
          }
        }
      }
    }
    this.playerScore = playerScore;
    this.dealerScore = dealerScore;
  }

  getCardValue(value: string) {
    if (value === 'J' || value === 'Q' || value === 'K') {
      return 10;
    } else if (value === 'A') {
      return 11;
    } else {
      return parseInt(value);
    }
  }


}
