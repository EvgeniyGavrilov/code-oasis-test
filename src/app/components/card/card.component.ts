import {Component, Input, OnInit} from '@angular/core';
import {Card} from "../../app.component";

@Component({
  selector: 'app-card[card]',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card!: Card
  @Input() isOpened: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

  getClass(card: Card, isOpened: boolean) {
    return isOpened ? `${card.suit.toLowerCase()} value_${card.value}` : `closed-card`
  }

}
